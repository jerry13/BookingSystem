# BookingSystem
利用gspread、oauth2client至google drive 抓取google試算表，進行資料分析。
並將結果回存至雲端硬碟中。

注意

    python內建使用unicode，而google api使用utf-8，在ypthon2.7中，會導致編碼不同而無法正確解譯中文！
    可參考下網址進行修改。
    https://github.com/burnash/gspread/issues/544
    
參考資料

    Google Spreadsheets and Python：
        https://www.twilio.com/blog/2017/02/an-easy-way-to-read-and-write-to-a-google-spreadsheet-in-python.html
    Google Spreadsheets Python API：
        https://github.com/burnash/gspread/blob/master/README.md
    
#coding=utf-8
from oauth2client.service_account import ServiceAccountCredentials
import difflib
import gspread
import time
import datetime
import sys
# if DEBUG = 1 then this is debug mode
DEBUG = 0
OUTCONTROL = False
# source file name
rule = 5
DATE = 0
fileName = "Booking System (回應)"
outSheetName = "output"
ConsolidateName = "consolidate"
ConsolidateName2 = "consolidate2"

def compare_ratio(sh, x, num):
    ratio = [[None] * (x-1) for i in range(x-1)]
    for i in range(1, x):
        for j in range(i, x):
            # 比對公司名稱
            ratio[i-1][j-1] = difflib.SequenceMatcher(None, sh[i][num], sh[j][num]).ratio()
            ratio[j-1][i-1] = ratio[i-1][j-1]
            # print(i, sh[i][compate_num], j, sh[j][compate_num], ratio[i])
    return ratio
def compare_date(date1,date2):
    # 注意在转换过程中date1和date2必须是str类型！！
    date1 = datetime.datetime.strptime(date1,'%Y/%m/%d').date()
    date2 = datetime.datetime.strptime(date2,'%Y/%m/%d').date()
    return (date1-date2).days

## 輸入參數 查詢天數 輸出 Debugmod
#GAP = 7
GAP = int(sys.argv[1])

if sys.argv[2] =="True":
    OUTCONTROL = True

DEBUG = int(sys.argv[3])

# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']
creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
client = gspread.authorize(creds)

# Find a workbook by name and open the first sheet
# Make sure you use the right name here.
sheet = client.open(fileName.decode('utf-8')).get_worksheet(0)
if DEBUG == 1:
    print(sheet.title)
    print("sheet title=", sheet.title, type(sheet.title))
    print("sheet id=", sheet.id, type(sheet.id))

print("get data!")
table = sheet.get_all_values()
row = table.__len__()
col = table[0].__len__()

if DEBUG == 1:
    print("real row=", row)
    print("real col=", col)

'''     
                    最終用戶資料
時間戳記    公司名稱    公司名稱(英文)    公司電話    mail    New/Renewal 產品名細
   0         14         15               19         22       24          25

'''

similarRatio = []# [[None] * row for i in range(row)]
compareNum = [14, 15, 19, 22, 24, 25]
print("computing ratio...")
# 計算相似率
for compare in compareNum:
    similarRatio.append(compare_ratio(table, row, compare))

allRatio = [[0.0] * (row-1) for i in range(row-1)]
# 將訂單相似率加總
for j in range(0, 6):
    for i in range(0, row-1):
        for k in range (0, row-1):
            allRatio[i][k] += similarRatio[j][i][k]

if DEBUG == 1:
    for i in range(0,row-1):
        print(allRatio[i])

# create ratio sheet
outSheet = client.open(fileName).worksheet(outSheetName)
if outSheet == None:
    client.open(fileName).add_worksheet(outSheetName, row+1, row+1)
    if DEBUG == 1:
        print("create sheet",outSheetName)
print("writing ratio...")
# add ratio data
for i in range(0, row - 1):
    if (OUTCONTROL): outSheet.append_row(allRatio[i])
    if DEBUG == 1:
        for j in range(0, row - 1):
            if allRatio[i][j] >= rule :

                print("第{i}比資料與第{j}比資料相似={ratio}".format(i=i,j=j,ratio=allRatio[i][j]))
        print("")

print("sorting data...")
# 排序、整理輸出格式
array = [[] for i in range(row-1)]
for i in range(0, row-1):
    for j in range(0, row-1):
        if allRatio[i][j] >= rule:
            array[i].append(j)

outlist = [[] for i in range(row-1)]
for i in range(0, row-1):
    if len(array[i])>1:
        #   取得包含別人的陣列
        for j in array[i]:
            if i <= j:
                outlist[i] += array[i]+array[j]
            else:
                break
# 清除空陣列
l = len(outlist)
for i in range(0, l):
    if len(outlist[l-i-1]) == 0:
        outlist.remove([])
for i in range(0, len(outlist)):
    outlist[i] = list(set(outlist[i]))
    outlist[i].sort()


# create Consolidate2 sheet
print("writing result1...")
ConsolidateSheet2 = client.open(fileName).worksheet(ConsolidateName2)
if ConsolidateSheet2 == None:
    client.open(fileName).add_worksheet(ConsolidateName2, row+1, row+1)
    if DEBUG == 1:
        print("create sheet", ConsolidateName2)

for i in range(0,len(outlist)):
    for j in outlist[i]:
        if (OUTCONTROL): ConsolidateSheet2.append_row(sheet.row_values(j+ 2))

for i in range(0,row-1):
    if i not in outlist[0]:
        if (OUTCONTROL): ConsolidateSheet2.append_row(sheet.row_values(i + 2))


if DEBUG == 1:
    for i in range(len(outlist)):
        print(outlist[i], len(outlist[i]))

# 格式化成2016-03-20形式 <type 'str'>
today = time.strftime("%Y/%m/%d", time.localtime())
if DEBUG == 1:
    print("today=",today)
# 取得重複群組最後一筆資料
# 比對時間，超過一個星期則刪除
l = len(outlist)
delNum = 0
for i in range(l):
    getDate = str(table[outlist[i-delNum][-1] + 1][DATE].split()[0])
    if compare_date(today, getDate) > GAP:
        if DEBUG == 1:
            # remove data.
            print("刪除該筆資料",outlist[i-delNum])
        del outlist[i-delNum]
        delNum += 1
    else:
        if DEBUG == 1:
            # do nothing.
            print("保留資料並進行運算",getDate)

print("writing result2...")
# create Consolidate sheet
ConsolidateSheet = client.open(fileName).worksheet(ConsolidateName)
if ConsolidateSheet == None:
    if (OUTCONTROL): client.open(fileName).add_worksheet(ConsolidateName, row+1, row+1)
    if DEBUG == 1:
        print("create sheet", ConsolidateName)

count = 1
for i in range(0,len(outlist)):
    for j in range(len(outlist[i])):
        if (OUTCONTROL): ConsolidateSheet.append_row(sheet.row_values(outlist[i][j-1]+2))
        if (OUTCONTROL): ConsolidateSheet.update_cell(count, 1, i + 1)
        count += 1

print("finish!")
